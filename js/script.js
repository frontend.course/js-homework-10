const tabs = document.querySelector(".tabs");
const tabsContents = document.querySelectorAll(".tabs-content li");

document.addEventListener("DOMContentLoaded", function () {
    const tabContentMap = {};
    for (let tabContent of tabsContents) {
        tabContent.style.display = "none";
        tabContentMap[tabContent.dataset.tabContent] = tabContent;
    }

    const activeTab = document.querySelector(".tabs li.active");
    if (activeTab) {
        tabContentMap[activeTab.dataset.tab].style.display = null;
    }

    tabs.addEventListener("click", function (event) {
        if (event.target.tagName.toLocaleLowerCase() !== "li") {
            return;
        }

        const tab = event.target;

        tabs.querySelectorAll("li").forEach(function (element) {
            element.classList.remove("active");
        });

        tabsContents.forEach(function (element) {
            element.style.display = "none";
        });

        tab.classList.add("active");
        if (tabContentMap[tab.dataset.tab]) {
            tabContentMap[tab.dataset.tab].style.display = null;
        }
    });
});
